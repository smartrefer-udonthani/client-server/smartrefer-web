import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KetAttachmentTypeService } from './ket-attachment-type.service'
import { KetAttachmentService } from './ket-attachment.service'
import { KetBarthelRateService } from './ket-barthel-rate.service'
import { KetCauseReferbackService } from './ket-cause-referback.service'
import { KetCauseReferouteService } from './ket-cause-referoute.service'
import { KetCoOfficeService } from './ket-co-office.service'
import { KetEvaluateService } from './ket-evaluate.service'
import { KetLoadsService } from './ket-loads.service'
import { KetPersonImcService } from './ket-person-imc.service'
import { KetReferLogService } from './ket-refer-log.service'
import { KetReferResultService } from './ket-refer-result.service'
import { KetReferbackService } from './ket-referback.service'
import { KetReferoutService } from './ket-referout.service'
import { KetServiceplanService } from './ket-serviceplan.service'
import { KetStationService } from './ket-station.service'
import { KetStrengthService } from './ket-strength.service'
import { KetThaiaddressService } from './ket-thaiaddress.service'
import { KetTypeptService } from './ket-typept.service'
import { LoginService } from './login.service'
import { ServicesService } from './services.service'
// import { UploadService } from './upload.service'
import { VersionService } from './version.service'

@NgModule({
  declarations: [
    
  ],
  imports: [
    CommonModule
  ],
  providers: [
    KetAttachmentTypeService,
    KetAttachmentService,
    KetBarthelRateService,
    KetCauseReferbackService,
    KetCauseReferouteService,
    KetCoOfficeService,
    KetEvaluateService,
    KetLoadsService,
    KetPersonImcService,
    KetReferLogService,
    KetReferResultService,
    KetReferbackService,
    KetReferoutService,
    KetServiceplanService,
    KetStationService,
    KetStrengthService,
    KetThaiaddressService,
    KetTypeptService,
    LoginService,
    ServicesService,
    // UploadService,
    VersionService
  ]
})
export class SharedModule { }
