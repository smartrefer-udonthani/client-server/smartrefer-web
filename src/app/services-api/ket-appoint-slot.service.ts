import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class KetAppointSlotService {
  token: any;
  httpOptions: any;

  constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
    this.token = sessionStorage.getItem('token');

    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.token
      })
    };
  }

  
  async appointslots(hcode:any) {
    const _url = `${this.apiUrl}/appoint_slot/select_hcode?hcode=${hcode}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }  

  async onSave(info:any) {
    const _url = `${this.apiUrl}/appoint_slot/insert`;
    return this.httpClient.post(_url,info,this.httpOptions).toPromise();
  }
  
  async onUpdate(info:any,hcode:any,clinic_code:any) {
    const _url = `${this.apiUrl}/appoint_slot/update?hcode=${hcode}&clinic_code=${clinic_code}`;
    return this.httpClient.put(_url,info,this.httpOptions).toPromise();
  }

  async getHoliday() {
    const _url = `${this.apiUrl}/holiday/list`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }

  async getCountAppoint(hcode:any,clinic_code:any,date:any) {
    const _url = `${this.apiUrl}/appoint/select_appointByhcode?hcode=${hcode}&clinic_code=${clinic_code}&appoint_date=${date}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }

  async onSaveAppoint(info:any) {
    const _url = `${this.apiUrl}/appoint/insert`;
    return this.httpClient.post(_url,info,this.httpOptions).toPromise();
  }

}