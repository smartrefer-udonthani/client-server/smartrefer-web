import { NgModule, LOCALE_ID } from '@angular/core';
import { environment } from 'src/environments/environment';
import { DecimalPipe } from '@angular/common';

import { FormsModule } from '@angular/forms';


import { BreadcrumbModule } from 'xng-breadcrumb';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';



// primen NG
import { DialogModule } from 'primeng/dialog';
import { AccordionModule } from 'primeng/accordion';     //accordion and accordion tab
import { CalendarModule } from 'primeng/calendar';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { CheckboxModule } from 'primeng/checkbox';
import { RadioButtonModule } from 'primeng/radiobutton';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { AutoCompleteModule } from "primeng/autocomplete";
import { DropdownModule } from "primeng/dropdown";
import { InputMaskModule } from "primeng/inputmask";
import { InputNumberModule } from "primeng/inputnumber";
import { MultiSelectModule } from "primeng/multiselect";
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { CardModule } from 'primeng/card';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { SplitterModule } from "primeng/splitter";
import { SidebarModule } from 'primeng/sidebar';
import { PanelMenuModule } from 'primeng/panelmenu';
import { AvatarModule } from 'primeng/avatar';
import { PanelModule } from 'primeng/panel';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { ListboxModule } from 'primeng/listbox';
import { GalleriaModule } from 'primeng/galleria';
import { PaginatorModule } from 'primeng/paginator';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService, MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import { TieredMenuModule } from 'primeng/tieredmenu';
import { MenuModule } from 'primeng/menu';
import { DataViewModule } from 'primeng/dataview';
import { RatingModule } from 'primeng/rating';
import { BlockUIModule } from 'primeng/blockui';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { ProgressBarModule } from 'primeng/progressbar';
import { DividerModule } from 'primeng/divider';
// import { CustomTooltip } from 'src/app/shared/custom-tooltip.service';
// import { TooltipModule } from 'ng2-tooltip-directive';
// import { TextMaskModule } from 'angular2-text-mask';
// import {BreadcrumbModule} from 'primeng/breadcrumb';
import { MenuItem } from 'primeng/api';
import { CarouselModule } from 'primeng/carousel';
import { DockModule } from 'primeng/dock';
import { TableModule } from 'primeng/table';
import { MenubarModule } from 'primeng/menubar';
import { ImageModule } from 'primeng/image';
import { SelectButtonModule } from 'primeng/selectbutton';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { BadgeModule } from 'primeng/badge';
import { InputSwitchModule } from 'primeng/inputswitch';
import { TagModule } from 'primeng/tag';
import { FieldsetModule } from 'primeng/fieldset';
import { ChartModule } from 'primeng/chart';
import { TabViewModule } from 'primeng/tabview';
import { ScrollTopModule } from 'primeng/scrolltop';
import { ChipModule } from 'primeng/chip';

// Angular Material
import { MatIconModule } from '@angular/material/icon';
import { MatCommonModule,MAT_DATE_LOCALE,MatNativeDateModule, DateAdapter } from '@angular/material/core';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';


// import { AggridLocaleService } from 'src/app/shared/ag-grid-locale.service';

import { ThaiDatePipe } from '../pipes/to-thai-date-pipe';
import { ThaiAgePipe } from '../pipes/to-thai-age.pipe';
import { ClipboardModule } from 'ngx-clipboard';
import {TextMaskModule} from 'angular2-text-mask';
import { TimeMaskDirective } from '../../app/components/directives/time-mask.directive';
import { ReactiveFormsModule } from '@angular/forms';
import { registerLocaleData } from '@angular/common';
import localeTh from '@angular/common/locales/th';
registerLocaleData(localeTh);





@NgModule({
  declarations: [ThaiDatePipe, ThaiAgePipe,TimeMaskDirective],
  imports: [MatDatepickerModule,NgxMaterialTimepickerModule,ReactiveFormsModule,TextMaskModule],

  exports: [BreadcrumbModule, ButtonModule, InputTextModule,MatFormFieldModule,TimeMaskDirective,ReactiveFormsModule,TextMaskModule,
    AccordionModule, CalendarModule, DialogModule, TableModule,
    CheckboxModule, RadioButtonModule, InputTextareaModule,
    MessagesModule, MessageModule, ScrollPanelModule, SplitterModule, SidebarModule,
    PanelMenuModule, AvatarModule, CardModule, AutoCompleteModule,
    DropdownModule, InputMaskModule, InputNumberModule, PanelModule,
    OverlayPanelModule, ListboxModule, GalleriaModule,
    MultiSelectModule, PaginatorModule, ConfirmDialogModule, ToastModule, MenubarModule,
    TieredMenuModule, MenuModule, DataViewModule,
    RatingModule, BreadcrumbModule, MatDatepickerModule, MatNativeDateModule, MatInputModule,
    ProgressBarModule, DividerModule, MatIconModule,
    MatCommonModule, MatButtonModule, BlockUIModule, ProgressSpinnerModule, CarouselModule, TagModule
    // TooltipModule,
    // TextMaskModule,
    , DockModule, ImageModule, SelectButtonModule, ToggleButtonModule, BadgeModule, ThaiDatePipe, ThaiAgePipe, InputSwitchModule, FormsModule, MatSelectModule, FieldsetModule,
    ChartModule, TabViewModule, ScrollTopModule, ClipboardModule, ChipModule,NgxMaterialTimepickerModule

  ],
  providers: [
    { provide: ConfirmationService },
    { provide: MessageService },
    { provide: MatDatepickerModule },
    { provide: MatNativeDateModule },
    DecimalPipe,
    { provide: MAT_DATE_LOCALE, useValue: 'th-TH' },
    { provide: LOCALE_ID, useValue: 'th-TH' }
    //   { provide: 'limitData', useValue: environment.limitDataimport },


  ],
})
export class SharedModule { }