import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {SharedModule} from '../../shared/sharedModule';
import { HomeRoutes } from './home-routing.module';


import { ReferoutComponent } from './referout/referout.component';
import { ReferinComponent } from './referin/referin.component';
import  {ReferbackComponent} from './referback/referback.component';

import { ReferreceiveComponent } from './referreceive/referreceive.component';
import { AppointComponent } from './appoint/appoint.component';
import { EvaluateComponent } from './evaluate/evaluate.component';
import { PhrComponent } from './phr/phr.component';
import { ImcComponent } from './imc/imc.component';
import { ReportComponent } from './report/report.component';
import { AnywhereComponent } from './anywhere/anywhere.component';
import { VersionComponent } from './version/version.component';

import { ReferoutCreateComponent } from './referout-create/referout-create.component';
import { ReferoutViewsComponent } from './referout-views/referout-views.component';

import { ReferbackCreateComponent } from './referback-create/referback-create.component';
import { ReferbackViewsComponent } from './referback-views/referback-views.component';
import { ReferreceiveViewsComponent } from './referreceive-views/referreceive-views.component';
import { ImcViewsComponent } from './imc-views/imc-views.component';
import { ImcSaveComponent } from './imc-save/imc-save.component';
import { UploadsComponent } from './uploads/uploads.component';
import { ReferinViewsComponent } from './referin-views/referin-views.component';
import { ReferoutViewsOnlyComponent } from './referout-views-only/referout-views-only.component';
import { ReferbackViewsOnlyComponent } from './referback-views-only/referback-views-only.component';
import { PrintreferoutComponent } from './printreferout/printreferout.component';
import { PrintreferbackComponent } from './printreferback/printreferback.component';
import { AssessViewComponent } from './assess-view/assess-view.component';
import { AssessComponent } from './assess/assess.component';

// import {NgxPrintModule} from 'ngx-print';
import { HelpdeskComponent } from './helpdesk/helpdesk.component';
import { UploadissueComponent } from './uploadissue/uploadissue.component';
import {InitTableServcie} from '../../service/inittable.service';
import {TelemedComponent} from './telemed/telemed.component';
import {TelemedViewsComponent} from './telemed-views/telemed-views.component';
import { TesteComponent } from './teste/teste.component';
import { TelemedViewsOnlyComponent } from './telemed-views-only/telemed-views-only.component';
import { AppointSlotsComponent } from './appoint-slots/appoint-slots.component';
import { PrintappointComponent } from './printappoint/printappoint.component';

import { MultiSelectModule } from 'primeng/multiselect';
import { ReactiveFormsModule } from '@angular/forms';
@NgModule({
  declarations: [   
    ReferoutComponent,
    ReferinComponent,
    ReferbackComponent,
    VersionComponent,   
    ReferoutComponent,
    ReferinComponent,
    ReferbackComponent,
    ReferreceiveComponent,
    AppointComponent,
    EvaluateComponent,
    PhrComponent,
    ImcComponent,
    ReportComponent,
    AnywhereComponent,
    ReferoutCreateComponent,
    ReferoutViewsComponent,    
    ReferbackCreateComponent,
    ReferbackViewsComponent,
    ReferreceiveViewsComponent,
    ImcViewsComponent,
    ImcSaveComponent,
    UploadsComponent,
    ReferinViewsComponent,
    ReferoutViewsOnlyComponent,
    ReferbackViewsOnlyComponent,
    PrintreferoutComponent,
    PrintreferbackComponent,
    AssessViewComponent,
    AssessComponent,
    HelpdeskComponent,
    UploadissueComponent,
    TelemedComponent,
    TelemedViewsComponent,
    TesteComponent,
    TelemedViewsOnlyComponent,  
    AppointSlotsComponent,
    PrintappointComponent
   
  ],
  imports: [
    CommonModule,
    // NgxPrintModule,

    SharedModule,
    RouterModule.forChild(HomeRoutes),
    MultiSelectModule,ReactiveFormsModule

  ],

  providers: [InitTableServcie]
})
export class HomeModule { }
