import * as moment from 'moment-timezone';
import { Component, OnInit,Input, ViewChild} from '@angular/core';
import { Router,NavigationExtras } from '@angular/router';
import {slideInOutAnimation,fadeInAnimation} from 'src/app/animations/index';
import { Table } from 'primeng/table';
import { PrimeNGConfig } from 'primeng/api';
import { KetPersonImcService } from '../../../services-api/ket-person-imc.service';
import { GlobalVariablesService } from 'src/app/shared/globalVariables.service';
import { ServicesService } from '../../../services-api/services.service';

import { AlertService } from '../../../service/alert.service';
// import { flatten } from '@angular/compiler';

@Component({
  selector: 'app-imc',
  templateUrl: './imc.component.html',
  styleUrls: ['./imc.component.css'],
  animations: [slideInOutAnimation],
  host: { '[@slideInOutAnimation]': '' }
})
export class ImcComponent implements OnInit {
  scrHeight: number = 0;
  scrWidth: number = 0;
  boxHeight: any;
  rowsData: any[] = [];
  device:any = 't';
  
  // today: any = moment(Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
  today: Date = new Date();

  blockedPanel: boolean = false;
  

  // customers: Customer[] = [];

  // selectedCustomers: Customer[] = [];

  // representatives: Representative[] = [];
  selectedRowsData:any = [];
 
  statuses: any[] = [];

  loading: boolean = true;

  displayCreate: boolean = false;
  rowsDataCreate: any[] = [];
  loadingCreate: boolean = true;

  displayReferOut: boolean = false;
  rowsDataReferOut: any[] = [];
  loadingReferOut: boolean = true;

  @ViewChild('dt') table!: Table;

  @ViewChild('dtCreate') tableCreate!: Table;

  first: any = 0;

  rows: any = 10;

  hcode: any;

  sdate: Date;
  edate: Date;

  sdateCreate: any;
  edateCreate: any;

  limitReferOut: any = 2000;


  buttonClassStrength: string = 'p-button-rounded p-button-warning';
  value3: any;
  justifyOptions: any = [];
  value1: string = "off";
  stateOptions: any = [];

  ptableStyle: any = {
    width: '100%',
    height: '100%',
    flex: '1 1 auto'
  };
  scrollHeight: string = '';
  validateForm:boolean = true;
  
  selectedStatus = '0';

  constructor(
    
    private primengConfig: PrimeNGConfig,
    private ketPersonImcService: KetPersonImcService,
    private globalVariablesService: GlobalVariablesService,
    private servicesService: ServicesService,
    private router: Router,
    private alertService: AlertService,
  ) {
    
    this.hcode = sessionStorage.getItem('hcode');

    this.edate = this.today;
    
    let ssdate = parseInt(moment(this.today).tz('Asia/Bangkok').format('MM'));

    if(ssdate > 0 && ssdate < 10){
      let sd =  parseInt(moment(this.today).tz('Asia/Bangkok').format('YYYY'))-1+"-10-01";
      this.sdate =  new Date(sd);
 
    }else{
      this.sdate = this.today;
    }


    this.sdateCreate = this.today;
    this.edateCreate = this.today;

   }
   readGlobalValue() {
    this.scrHeight = Number(this.globalVariablesService.scrHeight);
    this.scrWidth = Number(this.globalVariablesService.scrWidth);
    this.boxHeight = ((this.scrHeight) - 80) + 'px';
    this.scrollHeight = (Number(this.globalVariablesService.scrHeight) - 360) + 'px';
    this.ptableStyle = {
      width: '100%',          
      height: (this.scrHeight - 280) + 'px'
    }
    // do something with the value read out
  }

  onSearch() {

    this.justifyOptions = [
      { icon: 'pi pi-align-left', justify: 'Left' },
      { icon: 'pi pi-align-right', justify: 'Right' },
      { icon: 'pi pi-align-center', justify: 'Center' },
      { icon: 'pi pi-align-justify', justify: 'Justify' }
    ];
    this.stateOptions = [
      { label: "Off", value: "off" },
      { label: "On", value: "on" }
    ];
    this.getInfo();
  }

  onClick(i:never){

  let jsonitem = JSON.stringify(i)
  sessionStorage.setItem('itemStorage', jsonitem); // บันทึกค่า Session Storage
  this.router.navigate(['/home/imcviews'])
  }


  ngOnInit(): void {

    this.readGlobalValue();
    this.getInfo();
    this.primengConfig.ripple = true;

  }

  onActivityChange(event: any) {
    const value = event.target.value;
    if (value && value.trim().length) {
      const activity = parseInt(value);

      if (!isNaN(activity)) {
        this.table.filter(activity, 'activity', 'gte');
      }
    }
  }

  onDateSelect(value: any) {
    this.table.filter(this.formatDate(value), 'date', 'equals')
  }

  formatDate(date: any) {
    let month = date.getMonth() + 1;
    let day = date.getDate();

    if (month < 10) {
      month = '0' + month;
    }

    if (day < 10) {
      day = '0' + day;
    }

    return date.getFullYear() + '-' + month + '-' + day;
  }

  onRepresentativeChange(event: any) {
    this.table.filter(event.value, 'representative', 'in')
  }

  reset() {
    this.first = 0;
  }

  isLastPage(): boolean {
    return this.rowsData ? this.first === (this.rowsData.length - this.rows) : true;
  }

  isFirstPage(): boolean {
    return this.rowsData ? this.first === 0 : true;
  }
  next() {
    this.first = this.first + this.rows;
  }

  prev() {
    this.first = this.first - this.rows;
  }

  async getInfo() {
    this.loading = true;
    this.rowsData = [];
    
    let s_date = moment(this.sdate).tz('Asia/Bangkok').format('YYYY-MM-DD');
    let e_date = moment(this.edate).tz('Asia/Bangkok').format('YYYY-MM-DD');
    
    try {
      let rs: any = await this.ketPersonImcService.select_hcode_status(this.hcode,this.selectedStatus,s_date,e_date);

      let item: any = rs[0];

      if (item) {

        this.rowsData = rs;
        this.loading = false;
      } else {
        this.loading = false;
      }
    } catch (error) {
      console.log(error);
      this.loading = false;
    }
  }

  onRowSelect(even:any){
    sessionStorage.setItem('itemStorage',JSON.stringify(even.data));
    this.router.navigate(['/home/imcviews'])  
    

  }
  onDateSelected(){
    if(this.edate < this.sdate){     
      this.alertService.error("ข้อผิดพลาด !", "วันเริ่มต้น มากกว่า วันสิ้นสุด ไม่ได้");
      this.validateForm = false;
    }else{
      this.validateForm = true;
    }
  }

}
