import * as moment from 'moment-timezone';
import { Component, OnInit,Input, ViewChild} from '@angular/core';
import { Router,NavigationExtras } from '@angular/router';
import {slideInOutAnimation,fadeInAnimation} from 'src/app/animations/index';
// import swal from 'sweetalert';


import { Table } from 'primeng/table';
import { PrimeNGConfig } from 'primeng/api';
import { KetPersonImcService } from '../../../services-api/ket-person-imc.service';
import { GlobalVariablesService } from 'src/app/shared/globalVariables.service';
import { ServicesService } from '../../../services-api/services.service';

import { AlertService } from '../../../service/alert.service';
import { KetBarthelRateService } from '../../../services-api/ket-barthel-rate.service';
import { ThaiDatePipe } from 'src/app/pipes/to-thai-date-pipe';

@Component({
  selector: 'app-imc-save',
  templateUrl: './imc-save.component.html',
  styleUrls: ['./imc-save.component.css'],
  animations: [slideInOutAnimation],
  // attach the fade in animation to the host (root) element of this component
  host: { '[@slideInOutAnimation]': '' }
})
export class ImcSaveComponent implements OnInit {
  userReport:any;
  itemStorage:any=[];
  itemRegister:any=[];
  itemCid:any;
  scrHeight: number = 0;
  scrWidth: number = 0;
  boxHeight: any;
  rowsData: any[] = [];
  device:any = 't';
  
  today: any = moment(Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');

  blockedPanel: boolean = false;
  

  // customers: Customer[] = [];

  // selectedCustomers: Customer[] = [];

  // representatives: Representative[] = [];
  selectedRowsData:any = [];

  statuses: any[] = [];

  loading: boolean = true;

  displayCreate: boolean = false;
  rowsDataCreate: any[] = [];
  loadingCreate: boolean = true;

  displayReferOut: boolean = false;
  rowsDataReferOut: any[] = [];
  loadingReferOut: boolean = true;

  @ViewChild('dt') table!: Table;

  @ViewChild('dtCreate') tableCreate!: Table;

  first: any = 0;

  rows: any = 10;

  hcode: any;

  personimc: any;

  sdate: Date;
  edate: Date;
  
  sdateCreate: any;
  edateCreate: any;

  limitReferOut: any = 2000;

  itemeparams:any;

  imcName:any=[];

  buttonClassStrength: string = 'p-button-rounded p-button-warning';
  value3: any;
  justifyOptions: any = [];
  value1: string = "off";
  stateOptions: any = [];

  ptableStyle: any = {
    width: '100%',
    height: '100%',
    flex: '1 1 auto'
  };
  scrollHeight: string = '';
  validateForm:boolean = false;
  refer_no:any;
  reg_date:any;
  imc_id:any;
  fullname:any;
  fulldata:any;
  cid:any;
  reg_by:any;
  dataImc:any;

  adl_total:any; 
  adl_total_text:any;

  username:any;
  hospmain:any;

  feeding:any = 0;
  grooming:any = 0;
  transfer:any = 0;
  toilet_use:any = 0;
  mobility:any=0;
  dressing:any=0;
  stairs:any=0;
  bathing:any=0;
  bowels:any=0;
  bladder:any=0;

  swallow:any;
  speech:any;
  neurogenic_bladder:any;
  eurogenic_bowel:any;
  neuropathic_pain:any;
  cognitive_impairment:any;

  constructor(
            
    private primengConfig: PrimeNGConfig,
    private ketPersonImcService: KetPersonImcService,
    private globalVariablesService: GlobalVariablesService,
    private servicesService: ServicesService,
    private router: Router,
    private alertService: AlertService,
    private ketBarthelRateService: KetBarthelRateService
  ) {
            
    this.hcode = sessionStorage.getItem('hcode');
    this.username = sessionStorage.getItem('username');
    this.userReport = sessionStorage.getItem('fullname');
    this.itemCid = sessionStorage.getItem('itemCid');
    let i: any = sessionStorage.getItem('itemStorage');
    this.itemStorage = JSON.parse(i);
    this.sdate = this.today;
    this.edate = this.today;

    this.sdateCreate = this.today;
    this.edateCreate = this.today;
   }

  ngOnInit(): void {

    let cid:any = this.itemStorage.cid || this.itemCid ;
    
    this.getInfo(cid);
    this.readGlobalValue();
    this.primengConfig.ripple = true;
    this.valInput();
    
  }

  readGlobalValue() {
    this.scrHeight = Number(this.globalVariablesService.scrHeight);
    this.scrWidth = Number(this.globalVariablesService.scrWidth);
    this.boxHeight = ((this.scrHeight) - 80) + 'px';
    this.scrollHeight = (Number(this.globalVariablesService.scrHeight) - 400) + 'px';
    this.ptableStyle = {
      width: (this.scrWidth-70) + 'px',
      
      height: (this.scrHeight - 300) + 'px'
    }
    // do something with the value read out
  }
   
  onActivityChange(event: any) {
    const value = event.target.value;
    if (value && value.trim().length) {
      const activity = parseInt(value);

      if (!isNaN(activity)) {
        this.table.filter(activity, 'activity', 'gte');
      }
    }
  }

  onDateSelect(value: any) {
    this.table.filter(this.formatDate(value), 'date', 'equals')
  }

  formatDate(date: any) {
    let month = date.getMonth() + 1;
    let day = date.getDate();

    if (month < 10) {
      month = '0' + month;
    }

    if (day < 10) {
      day = '0' + day;
    }

    return date.getFullYear() + '-' + month + '-' + day;
  }

  onRepresentativeChange(event: any) {
    this.table.filter(event.value, 'representative', 'in')
  }

  reset() {
    this.first = 0;
  }

  isLastPage(): boolean {
    return this.rowsData ? this.first === (this.rowsData.length - this.rows) : true;
  }

  isFirstPage(): boolean {
    return this.rowsData ? this.first === 0 : true;
  }
  next() {
    this.first = this.first + this.rows;
  }

  prev() {
    this.first = this.first - this.rows;    
  }

  async getInfo(cid:any) {
    try {
      let rs: any = await this.ketPersonImcService.select_cid(cid);
      let e:any = rs[0];
      if(!rs[0]){
        
        this.registerImc(cid);

      }else{
        this.refer_no=e.refer_no;
        this.hospmain = e.hospmain;
        this.reg_date=e.reg_date;
        this.imc_id=e.imc_id;
        this.cid=e.cid;
        this.reg_by=e.reg_by;
        this.getImcName(e.cid);

      }
    } catch (error) {
      console.log(error);
    }
  }

async getImcName(cid:any){
  try {
    let rs: any = await this.ketPersonImcService.person_cid(cid);
    this.fullname=rs[0].fullname
  }  
  catch (error) {
    console.log(error);
    if(error.error.salMessage){
      this.alertService.error(error.error.salMessage);
    }
  }
}

  async registerImc(cid:any){
    let item:any = {}
      let info = {
          "refer_no": this.itemStorage.refer_no,
          "hcode": this.hcode,
          "hospmain": this.itemStorage.refer_hospcode || this.itemStorage.hospmain,
          "cid": cid,
          "reg_date": moment(Date()).tz('Asia/Bangkok').format('YYYY-MM-DD'),
          "reg_by": this.userReport,
          "reg_by_username": this.username,
          "status": "0"
    
    }
    item.rows = info;
    

    try {
      let rs:any = await this.ketPersonImcService.select_register(item,cid);
      let i:any = rs[0];
      this.refer_no=i.refer_no;
      this.hospmain = i.hospmain;
      this.reg_date=i.reg_date;
      this.imc_id=i.imc_id;
      this.cid=i.cid;
      this.reg_by=i.reg_by;
      this.getImcName(i.cid);

    } catch (error) {
      console.log(error);
      if(error.error.salMessage){
        this.alertService.error(error.error.salMessage);
      }  
    }
  }

  async onSave(){

  let swallow_
  let speech_
  let neurogenic_bladder_
  let eurogenic_bowel_
  let neuropathic_pain_
  let cognitive_impairment_
    if(this.swallow) {
      swallow_ = this.swallow[0]
    }else{
      swallow_ = 0
    }

    if(this.speech) {
      speech_ = this.speech[0]
    }else{
      speech_ = 0
    }

    if(this.neurogenic_bladder) {
      neurogenic_bladder_ = this.neurogenic_bladder[0]
    }else{
      neurogenic_bladder_ = 0
    }

    if(this.eurogenic_bowel) {
      eurogenic_bowel_ = this.eurogenic_bowel[0]
    }else{
      eurogenic_bowel_ = 0
    }

    if(this.neuropathic_pain) {
      neuropathic_pain_ = this.neuropathic_pain[0]
    }else{
      neuropathic_pain_ = 0
    }

    if(this.cognitive_impairment) {
      cognitive_impairment_ = this.cognitive_impairment[0]
    }else{
      cognitive_impairment_ = 0
    }


    var data_person_imc = {
      "rows" : {
          "imc_id":this.imc_id,
          "brt_date":this.today,
          "hcode":this.hcode,
          "reg_by":this.reg_by,
          "feeding_score":`${this.feeding}`,
          "transfer_score":`${this.transfer}`,
          "grooming_score":`${this.grooming}`,
          "toilet_score":`${this.toilet_use}`,
          "bathing_score":`${this.bathing}`,
          "mobility_score":`${this.mobility}`,
          "stairs_score":`${this.stairs}`,
          "dressing_score":`${this.dressing}`,
          "bowels_score":`${this.bowels}`,
          "bladder_score":`${this.bladder}`,
          "adl_total":`${this.adl_total}`,
          "adl_result":`${this.adl_total_text}`,
          "swallow":`${swallow_}`,
          "speech":`${speech_}`,
          "neurogenic_bladder":`${neurogenic_bladder_}`,
          "eurogenic_bowel":`${eurogenic_bowel_}`,
          "neuropathic_pain":`${neuropathic_pain_}`,
          "cognitive_impairment":`${cognitive_impairment_}`
     }
  }

  try {
    let rs:any = await this.ketBarthelRateService.onSave(data_person_imc);
    this.router.navigate(['/home/imcviews'])
    
  } catch (error) {
    console.log(error);
    if(error.error.salMessage){
      this.alertService.error(error.error.salMessage);
    }
  }

  }

  async valInput(){
    this.adl_total = this.feeding + this.grooming + this.transfer + this.toilet_use + this.mobility + this.dressing + this.stairs + this.bathing + this.bowels + this.bladder

    await this.adlTotal(this.adl_total);

  }

  async adlTotal(adl_total:any){
    if (adl_total >= 0 && adl_total <= 4) {
      var adl_text = "กลุ่มที่ 3 (ติดเตียง) พึ่งตนเองไม่ได้ ช่วยเหลือตนเองไม่ได้ พิการหรือทุพพลภาพ";
      this.adl_total_text = adl_text
  } else if (adl_total >= 5 && adl_total <= 11) {
      var adl_text = "กลุ่มที่ 2 (ติดบ้าน) ดูแลตนเองได้บ้าง ช่วยเหลือตนเองได้บ้าง";
      this.adl_total_text = adl_text
  } else {
      var adl_text = "กลุ่มที่ 1 (ติดสังคม) พึ่งตนเองได้ ช่วยเหลือผู้อื่น ชุมชนและสังคมได้";
      this.adl_total_text = adl_text
  }
  }
}
