
import {  Routes } from '@angular/router';

import { ReferoutComponent } from './referout/referout.component';
import { ReferinComponent } from './referin/referin.component';
import  {ReferbackComponent} from './referback/referback.component';

import { LayoutComponent } from './layout/layout.component'; 
import { VersionComponent } from './version/version.component'; 

import { ReferreceiveComponent } from './referreceive/referreceive.component';
import { AppointComponent } from './appoint/appoint.component';
import { EvaluateComponent } from './evaluate/evaluate.component';
import { PhrComponent } from './phr/phr.component';
import { ImcComponent } from './imc/imc.component';
import { ReportComponent } from './report/report.component';
import { AnywhereComponent } from './anywhere/anywhere.component';

import { ReferoutCreateComponent } from './referout-create/referout-create.component';
import { ReferbackCreateComponent } from './referback-create/referback-create.component';
import { ReferoutViewsComponent } from './referout-views/referout-views.component';
import { ReferbackViewsComponent } from './referback-views/referback-views.component';


import { ReferreceiveViewsComponent } from './referreceive-views/referreceive-views.component';
import { ImcViewsComponent } from './imc-views/imc-views.component';
import { ImcSaveComponent } from './imc-save/imc-save.component';
import { UploadsComponent } from './uploads/uploads.component';
import { ReferinViewsComponent } from './referin-views/referin-views.component';
import { ReferoutViewsOnlyComponent } from './referout-views-only/referout-views-only.component';
import { ReferbackViewsOnlyComponent } from './referback-views-only/referback-views-only.component';
import { PrintreferoutComponent } from './printreferout/printreferout.component';
import { PrintreferbackComponent } from './printreferback/printreferback.component';
import { AssessViewComponent } from './assess-view/assess-view.component';
import { AssessComponent } from './assess/assess.component';
import { HelpdeskComponent } from './helpdesk/helpdesk.component';
import { UploadissueComponent } from './uploadissue/uploadissue.component';
import {TelemedComponent} from './telemed/telemed.component';
import {TelemedViewsComponent} from './telemed-views/telemed-views.component';
import {TelemedViewsOnlyComponent} from './telemed-views-only/telemed-views-only.component';
import { AppointSlotsComponent } from './appoint-slots/appoint-slots.component';
import { PrintappointComponent } from './printappoint/printappoint.component';

import {TesteComponent} from './teste/teste.component'
// localStorage.setItem('routmain','/home');

let m = sessionStorage.getItem('routmain');

export const HomeRoutes: Routes = [

  {path: '', component: ReportComponent },
  
  {path: 'teste', component: TesteComponent},
  {path: 'referout', component: ReferoutComponent},
  {path: 'referin', component: ReferinComponent},
  {path: 'referback', component: ReferbackComponent},

  {path: 'layout', component: LayoutComponent },
  {path: 'version', component: VersionComponent},
  
  {path: 'referreceive', component: ReferreceiveComponent},
  {path: 'appoint', component: AppointComponent},
  {path: 'evalate', component: EvaluateComponent},
  {path: 'phr', component: PhrComponent},
  {path: 'imc', component: ImcComponent},
  {path: 'report', component: ReportComponent},
  {path: 'anywhere', component: AnywhereComponent},
  {path: 'helpdesk', component: HelpdeskComponent},
  {path: 'uploadissue', component: UploadissueComponent},
  {path: 'appoint-slosts', component: AppointSlotsComponent},

 
  {path: 'referback-create', component: ReferbackCreateComponent,data: { breadcrumb: 'referback-create' }},
  {path: 'referback-views', component: ReferbackViewsComponent},
  {path: 'referout-create', component: ReferoutCreateComponent, data: { breadcrumb: 'referout-create' }},
  {path: 'referout-views', component: ReferoutViewsComponent, data: { breadcrumb: 'referout-views' }},
  {path: 'referreceive-views', component: ReferreceiveViewsComponent, data: { breadcrumb: 'referreceive-views' }},
  {path: 'imcviews', component: ImcViewsComponent, data: { breadcrumb: 'imcviews' }},
  {path: 'referback-create', component: ReferbackCreateComponent, data: { breadcrumb: 'Referback-create' }},
  {path: 'imcsave', component: ImcSaveComponent, data: { breadcrumb: 'imcsave' }},
  {path: 'uploads', component: UploadsComponent, data: { breadcrumb: 'Uploads' }},
  {path: 'referin-views', component: ReferinViewsComponent},
  {path: 'referout-views-only', component: ReferoutViewsOnlyComponent},
  {path: 'referback-views-only', component: ReferbackViewsOnlyComponent},
  {path: 'printreferout', component: PrintreferoutComponent},
  {path: 'printreferback', component: PrintreferbackComponent},
  {path: 'assess-view', component: AssessViewComponent},
  {path: 'assess', component: AssessComponent},
  {path: 'telemed', component: TelemedComponent},
  {path: 'telemed-views', component: TelemedViewsComponent},
  {path: 'telemed-views-only', component: TelemedViewsOnlyComponent},
  {path: 'printappoint', component: PrintappointComponent},

  

  
];



