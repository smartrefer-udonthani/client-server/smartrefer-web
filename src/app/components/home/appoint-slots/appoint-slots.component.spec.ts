import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppointSlotsComponent } from './appoint-slots.component';

describe('AppointSlotsComponent', () => {
  let component: AppointSlotsComponent;
  let fixture: ComponentFixture<AppointSlotsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppointSlotsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppointSlotsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
