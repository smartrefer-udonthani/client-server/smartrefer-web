import { Injectable } from '@angular/core';
import { default as swal, SweetAlertOptions } from 'sweetalert2';

@Injectable({
    providedIn: 'root',
})
export class AlertService {
    constructor() {}

    error(text: any = '', title: any = 'เกิดข้อผิดพลาด', timer = null) {
        const option: SweetAlertOptions = {
            title: title,
            text: text,
            // type: 'error',
            confirmButtonText: 'ตกลง',
        };
        if (timer) {
            // option.timer = timer;
        }
        swal.fire(option);
    }

    info(text: any) {
        const option: SweetAlertOptions = {
            text: text,
            // type: 'info',
            confirmButtonText: 'ตกลง',
        };
        swal.fire(option);
    }

    success(text = '', title = 'ดำเนินการเสร็จเรียบร้อย') {
        const option: SweetAlertOptions = {
            title: title,
            text: text,
            timer: 500,
            // type: 'success',
            confirmButtonText: 'ตกลง',
        };
        swal.fire(option).then(
            function () {},
            // handling the promise rejection
            function (dismiss) {
                if (dismiss === 'timer') {
                }
            }
        );
    }
    successPro(text = '', title = 'ดำเนินการเสร็จเรียบร้อย') {
        const option: SweetAlertOptions = {
            title: title,
            text: text,
            timer: 2000,
            // type: 'success',
        };
        let timerInterval;
        swal.fire({
            title: 'ดำเนินการเสร็จเรียบร้อย',
            html: 'กำลังดำเนินการปิด <b></b> สักครู่.',
            timer: 500,
            timerProgressBar: true,
            didOpen: () => {
              swal.showLoading();
                const timer = swal.getPopup().querySelector('b');
                timerInterval = setInterval(() => {
                    timer.textContent = `${swal.getTimerLeft()}`;
                }, 100);
            },
            willClose: () => {
                clearInterval(timerInterval);
            },
        }).then((result) => {
            /* Read more about handling dismissals below */
            if (result.dismiss === swal.DismissReason.timer) {
                console.log('ตั้วเวลาปิดกรุณารอระบบ');
            }
        });
       
    }

    serverError() {
        const option: SweetAlertOptions = {
            title: 'เกิดข้อผิดพลาด',
            text: 'เกิดข้อผิดพลาดในการเชื่อมต่อกับเซิร์ฟเวอร์',
            // type: 'error',
            confirmButtonText: 'ตกลง',
        };
        swal.fire(option);
    }

    async confirm(text = 'คุณต้องการดำเนินการนี้ ใช่หรือไม่?') {
        const option: SweetAlertOptions = {
            title: '',
            text: text,
            // type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ใช่, ดำเนินการ!',
            cancelButtonText: 'ยกเลิก',
        };

        let result = await swal.fire(option);
        if (result.dismiss) return false;
        if (result.value) return true;

        return false;
    }
}
